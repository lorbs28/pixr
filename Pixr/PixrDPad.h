//
//  PixrDPad.h
//  Pixr
//
//  Created by Bryan Lor on 10/23/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//

// TODO: MIGHT not need the following 4
#define WALK_UP 1;
#define WALK_RIGHT 2;
#define WALK_DOWN 3;
#define WALK_LEFT 4;

#import <SpriteKit/SpriteKit.h>

@interface PixrDPad : SKSpriteNode
@end
