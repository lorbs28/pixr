//
//  PixrButton.m
//  Pixr
//
//  Created by Bryan Lor on 11/3/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//

#import "PixrButton.h"

@interface PixrButton ()
@property (nonatomic, strong) SKSpriteNode *button;
@property (nonatomic, assign) CGRect myRect;
@property (nonatomic, assign) CGFloat myRadius;
@end

@implementation PixrButton

- (id)initWithImageNamed:(NSString *)name
{
    self = [super initWithImageNamed:name];
    
    if (self) {
        _button = [SKSpriteNode spriteNodeWithImageNamed:name];
    }
    
    return self;
}

@end
