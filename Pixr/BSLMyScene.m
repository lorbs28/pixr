//
//  BSLMyScene.m
//  Pixr
//
//  Created by Bryan Lor on 10/16/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//
#import "BSLMyScene.h"
#import "PixrMap.h"
#import "PixrButton.h"
#import "PixrDPad.h"

@interface BSLMyScene ()

@property (nonatomic) SKNode *world;
@property (nonatomic) SKNode *gui;
@property (nonatomic) SKSpriteNode *player;
@property (nonatomic) SKSpriteNode *test;
@property (nonatomic) PixrButton *actionButton;
@property (nonatomic) PixrButton *actionButtonTwo;
@property (nonatomic) PixrDPad *upButton;
@property (nonatomic) PixrDPad *rightButton;
@property (nonatomic) PixrDPad *leftButton;
@property (nonatomic) PixrDPad *downButton;
@property (nonatomic) PixrMap *map;

@end

@implementation BSLMyScene

-(id)initWithSize:(CGSize)size {
    
    if (self = [super initWithSize:size]) {
        
        self.physicsWorld.gravity = CGVectorMake(0, 0);
        self.physicsWorld.contactDelegate = self;
        
        NSLog(@"Size height: %f, width: %f", size.height, size.width);
        
        self.world = [SKNode node];
        self.world.zPosition = 1.0;
        self.world.name = @"world";
        
        self.gui = [SKNode node];
        self.gui.zPosition = 2.0;
        self.gui.name = @"gui";
        
        self.backgroundColor = [SKColor colorWithRed:0.75 green:0.75 blue:0.75 alpha:1.0];
        
        // Map
        self.map = [[PixrMap alloc] initWithGridSize:CGSizeMake(20,20)];
        self.map.name = @"gameMap";
        self.map.zPosition = 1;
        self.map.maxFloorCount = 80;
        self.map.maxFloorTilerCount = 10;
        self.map.floorTilerSpawnProbability = 35;
        self.map.turnResistance = 35;
        self.map.anchorPoint = CGPointMake(0.5f, 0.5f);
        
        // Set the settings for room making before generating the map.
        self.map.roomProbability = 20;
        self.map.roomMinSize = CGSizeMake(2, 2);
        self.map.roomMaxSize = CGSizeMake(6, 6);
        self.map = [self.map generateWithMap:self.map];
        
        // Controls
        self.actionButton = [[PixrButton alloc] initWithImageNamed:@"button"];
        self.actionButton.position = CGPointMake(self.size.width - 60, 50.0f);
        self.actionButton.name = @"actionButtonNode";
        self.actionButton.zPosition = 2.0;
        
        self.actionButtonTwo = [[PixrButton alloc] initWithImageNamed:@"button"];
        self.actionButtonTwo.position = CGPointMake(self.size.width - 150, 50.0f);
        self.actionButtonTwo.name = @"actionButtonTwoNode";
        self.actionButtonTwo.zPosition = 2.1;
        
        // Down directional
        self.downButton = [[PixrDPad alloc] initWithImageNamed:@"directionalButton"];
        self.downButton.position = CGPointMake(112.5f, 37.5f);
        self.downButton.name = @"downButtonNode";
        self.downButton.zPosition = 2.2;
        
        // Left directional
        self.leftButton = [[PixrDPad alloc] initWithImageNamed:@"directionalButton"];
        self.leftButton.position = CGPointMake(37.5f, 75.0f);
        self.leftButton.name = @"leftButtonNode";
        self.leftButton.zPosition = 2.3;
        
        // Right directional
        self.rightButton = [[PixrDPad alloc] initWithImageNamed:@"directionalButton"];
        self.rightButton.position = CGPointMake(187.5f, 75.0f);
        self.rightButton.name = @"rightButtonNode";
        self.rightButton.zPosition = 2.4;
        
        // Up directional
        self.upButton = [[PixrDPad alloc] initWithImageNamed:@"directionalButton"];
        self.upButton.position = CGPointMake(112.5f, 112.5f);
        self.upButton.name = @"upButtonNode";
        self.upButton.zPosition = 2.5;
        
        // Player
        self.player = [[SKSpriteNode alloc] initWithImageNamed:@"pixr"];
        self.player.name = @"pixr";
        self.player.position = CGPointMake(self.map.spawnPoint.x, self.map.spawnPoint.y);
        self.player.physicsBody.allowsRotation = NO;
        self.player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.player.texture.size];
        self.player.physicsBody.categoryBitMask = CollisionTypePlayer;
        self.player.physicsBody.collisionBitMask = CollisionTypeWall;
        self.player.physicsBody.contactTestBitMask = CollisionTypeWall;
        self.player.physicsBody.usesPreciseCollisionDetection = YES;
        self.player.zPosition = 3;
        
        // PHYSICS TEST
        self.test = [[SKSpriteNode alloc] initWithImageNamed:@"pixr"];
        self.test.name = @"pixr2";
        self.test.position = CGPointMake(self.map.spawnPoint.x, self.map.spawnPoint.y);
        self.test.physicsBody.allowsRotation = NO;
        self.test.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.player.texture.size];
        self.test.physicsBody.categoryBitMask = CollisionTypeWall;
        self.test.physicsBody.collisionBitMask = CollisionTypePlayer;
        self.test.physicsBody.dynamic = NO;
        self.test.physicsBody.contactTestBitMask = CollisionTypePlayer;
        
        self.test.physicsBody.usesPreciseCollisionDetection = YES;
        self.test.zPosition = 3;
        
        NSLog(@"(%f,%f)", self.map.spawnPoint.x, self.map.spawnPoint.y);
        
        [self.gui addChild:self.downButton];
        [self.gui addChild:self.leftButton];
        [self.gui addChild:self.rightButton];
        [self.gui addChild:self.upButton];
        [self.gui addChild:self.actionButton];
        [self.gui addChild:self.actionButtonTwo];
        [self.map addChild:self.test];
        [self.map addChild:self.player];
        
        [self.world addChild:self.map];
        
        [self centerWorldOnPosition:self.map.spawnPoint];
        
        [self addChild:self.world];
        [self addChild:self.gui];
    
        NSLog(@"Player bit mask: (%u)",self.player.physicsBody.categoryBitMask);
        
    }
    return self;
}

-(void)didBeginContact:(SKPhysicsContact *)contact
{
    NSLog(@"Did I even get here?");
    
    SKPhysicsBody *firstBody, *secondBody;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    } else {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    if ((firstBody.categoryBitMask & CollisionTypePlayer) != 0 &&
        (secondBody.categoryBitMask & CollisionTypeWall) != 0)
    {
        [self didCollideWithWall:(SKSpriteNode *)secondBody.node];
    }
    
}

-(void)didCollideWithWall:(SKSpriteNode *)player
{
    NSLog(@"HIT!");
}

//-(BOOL)isCollidingAfterMovingWithPosition:(CGPoint)position
//{
//    __block BOOL isColliding;
//    
//    [self enumerateChildNodesWithName:@"wall" usingBlock:^(SKNode *node, BOOL *stop) {
//        SKSpriteNode *wall = (SKSpriteNode *)node;
//        
//        if (CGRectIntersectsRect(wall.frame, self.player.frame))
//        {
//            isColliding = NO;
//        } else {
//            isColliding = YES;
//        }
//    }];
//    
//    if (isColliding)
//    {
//        NSLog(@"Player is running into the wall.");
//    }
//    
//    
//    return isColliding;
//}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    //if fire button touched, bring the rain
    if ([node.name isEqualToString:@"actionButtonNode"]) {
        NSLog(@"Button touched!");
    } else if ([node.name isEqualToString:@"actionButtonTwoNode"]) {
        NSLog(@"Button 2 touched!");
    } else if ([node.name isEqualToString:@"downButtonNode"]) {
        
        NSLog(@"Down!");
        
        self.player.position = CGPointMake(self.player.position.x, self.player.position.y - 16);
        [self centerWorldOnPosition:self.player.position];
        
    } else if ([node.name isEqualToString:@"upButtonNode"]) {
        NSLog(@"Up!");
        
        self.player.position = CGPointMake(self.player.position.x, self.player.position.y + 16);
        [self centerWorldOnPosition:self.player.position];
        
    } else if ([node.name isEqualToString:@"leftButtonNode"]) {
        
        NSLog(@"Left!");
        
        self.player.position = CGPointMake(self.player.position.x - 16, self.player.position.y);
        [self centerWorldOnPosition:self.player.position];
        
    } else if ([node.name isEqualToString:@"rightButtonNode"]) {
        NSLog(@"Right!");
        
        // Check to see if the player is going to be colliding with the wall.
//        if (![self isCollidingAfterMovingWithPosition:self.player.position])
//        {
            self.player.position = CGPointMake(self.player.position.x + 16, self.player.position.y);
            [self centerWorldOnPosition:self.player.position];
//        }
        
    }
}

- (void)centerWorldOnPosition:(CGPoint)position {
    [self.world setPosition:CGPointMake(-(position.x) + CGRectGetMidX(self.frame),
                                        -(position.y) + CGRectGetMidY(self.frame))];
    
//    self.worldMovedForUpdate = YES;
}

//- (void)centerWorldOnCharacter:(APACharacter *)character {
//    [self centerWorldOnPosition:character.position];
//}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
}


@end
