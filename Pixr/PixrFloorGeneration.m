//
//  PixrFloorGeneration.m
//  Pixr
//
//  Created by Bryan Lor on 12/15/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//

#import "PixrFloorGeneration.h"

@implementation PixrFloorGeneration

- (instancetype) initWithCurrentPosition:(CGPoint)currentPosition andDirection:(NSUInteger)direction
{
    if (( self = [super init] ))
    {
        self.currentPosition = currentPosition;
        self.direction = direction;
    }
    return self;
}

@end
