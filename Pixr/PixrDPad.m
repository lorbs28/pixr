//
//  PixrDPad.m
//  Pixr
//
//  Created by Bryan Lor on 10/23/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//

#import "PixrDPad.h"

@interface PixrDPad ()

@property (nonatomic, strong) SKSpriteNode *directionalButton;
@property (nonatomic, assign) CGRect myRect;
@property (nonatomic, assign) CGFloat myRadius;

@end

@implementation PixrDPad

- (id)initWithImageNamed:(NSString *)name
{
    self = [super initWithImageNamed:name];
    
    if (self) {
        _directionalButton = [SKSpriteNode spriteNodeWithImageNamed:name];
    }
    
    return self;
}

@end
