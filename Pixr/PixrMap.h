//
//  PixrMap.h
//  Pixr
//
//  Created by Bryan Lor on 10/20/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//
typedef NS_ENUM(uint32_t, CollisionType)
{
    CollisionTypePlayer     = 0x1 << 0,
    CollisionTypeWall       = 0x1 << 1
};

#import <SpriteKit/SpriteKit.h>

@interface PixrMap : SKSpriteNode

@property (nonatomic) CGSize gridSize;
@property (nonatomic, readonly) CGPoint spawnPoint;
@property (nonatomic, readonly) CGPoint exitPoint;
@property (nonatomic) NSUInteger maxFloorCount;
@property (nonatomic) NSUInteger turnResistance;
@property (nonatomic) NSUInteger floorTilerSpawnProbability;
@property (nonatomic) NSUInteger maxFloorTilerCount;
@property (nonatomic) NSUInteger roomProbability;
@property (nonatomic) CGSize roomMinSize;
@property (nonatomic) CGSize roomMaxSize;

+(instancetype)mapWithGridSize:(CGSize)gridSize;
-(instancetype)initWithGridSize:(CGSize)gridSize;
-(PixrMap *)generateWithMap:(PixrMap *)incomingMap;

@end
