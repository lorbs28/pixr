//
//  BSLAppDelegate.h
//  Pixr
//
//  Created by Bryan Lor on 10/16/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
