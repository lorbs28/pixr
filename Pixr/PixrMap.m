//
//  PixrMap.m
//  Pixr
//
//  Created by Bryan Lor on 10/20/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//

#import "PixrMap.h"
#import "PixrMapTiles.h"
#import "PixrFloorGeneration.h"

@interface PixrMap ()
@property (nonatomic) PixrMapTiles *tiles;
@property (nonatomic) SKTextureAtlas *tileAtlas;
@property (nonatomic) CGFloat tileSize;
@property (nonatomic) NSMutableArray *floorTilers;
@property (nonatomic) PixrMap *currentMap;
@end

@implementation PixrMap

+(instancetype)mapWithGridSize:(CGSize)gridSize
{
    return [[self alloc] initWithGridSize:gridSize];
}

-(instancetype)initWithGridSize:(CGSize)gridSize
{
    if ((self = [super init]))
    {
        // Set the grid size.
        self.gridSize = gridSize;
        
        // Default settings for the variables that determine the cooridors and general map
        // generation.
        self.maxFloorCount = 100;
        self.turnResistance = 25;
        self.floorTilerSpawnProbability = 30;
        self.maxFloorTilerCount = 8;
        // Default settings for the variables that determine room sizes.
        self.roomProbability = 20;
        self.roomMinSize = CGSizeMake(3, 3);
        self.roomMaxSize = CGSizeMake(6, 6);
        
        _spawnPoint = CGPointZero;
        _exitPoint = CGPointZero;
        self.tileAtlas = [SKTextureAtlas atlasNamed:@"tiles"];
        
        NSArray *textureNames = [self.tileAtlas textureNames];
        SKTexture *tileTexture = [self.tileAtlas textureNamed:(NSString *)[textureNames firstObject]];
        self.tileSize = tileTexture.size.width;
        
        
    }
    
    return self;
}

/**
 * This method will generate the map grid with the tiles and walls.
 */
-(PixrMap *)generateWithMap:(PixrMap *)incomingMap
{
    
    self.currentMap = incomingMap;
    self.tiles = [[PixrMapTiles alloc] initWithGridSize:self.gridSize];
    [self generateTileGrid];
    [self generateWalls];
    [self generateTiles];
//    [self generateCollisionWalls];
    
    return self.currentMap;
}

- (void) addCollisionWallAtPosition:(CGPoint)position withSize:(CGSize)size
{
    SKSpriteNode *wall = [SKSpriteNode node];
    
    wall.position = position;
    wall.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:size];
    wall.physicsBody.categoryBitMask = CollisionTypeWall;
    wall.physicsBody.contactTestBitMask = CollisionTypePlayer;
    wall.physicsBody.collisionBitMask = CollisionTypeWall | CollisionTypePlayer;
    wall.physicsBody.usesPreciseCollisionDetection = YES;
    wall.physicsBody.dynamic = NO;
    
    [self.currentMap addChild:wall];
    
    NSLog(@"Collision wall built at: (%f,%f)",position.x, position.y);
}

/**
 * This method will determine how to generate the tile grid.
 */
-(void)generateTileGrid
{
    // Set the start point to the middle of the map for now.
    CGPoint startPoint = CGPointMake(self.tiles.gridSize.width / 2, self.tiles.gridSize.height / 2);
    
    // Set the tile type to the floor tile.
    [self.tiles setTileType:MapTileTypeFloor at:startPoint];
    
    // Set the current floor count to 1.
    __block NSUInteger currentFloorCount = 1;
    self.floorTilers = [NSMutableArray array];
    [self.floorTilers addObject:[[PixrFloorGeneration alloc] initWithCurrentPosition:startPoint andDirection:0]];
    
    // Instantiate a new floor generation
    
    
    while (currentFloorCount < self.maxFloorCount)
    {
        [self.floorTilers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
        {
            PixrFloorGeneration *floorTiler = (PixrFloorGeneration *)obj;
        
            {
                // Get a randon number between 1-4 for the direction.
                if (floorTiler.direction == 0 || [self randomNumberBetweenMin:0 andMax:100] <= self.turnResistance)
                {
                    floorTiler.direction = [self randomNumberBetweenMin:1 andMax:4];
                }
                
                
                
                CGPoint newPosition;
                
                // Determine which way the floor tiler should move.
                switch (floorTiler.direction)
                {
                    case 1: // Up
                        newPosition = CGPointMake(floorTiler.currentPosition.x, floorTiler.currentPosition.y - 1);
                        break;
                    case 2: // Down
                        newPosition = CGPointMake(floorTiler.currentPosition.x, floorTiler.currentPosition.y + 1);
                        break;
                    case 3: // Left
                        newPosition = CGPointMake(floorTiler.currentPosition.x - 1, floorTiler.currentPosition.y);
                        break;
                    case 4: // Right
                        newPosition = CGPointMake(floorTiler.currentPosition.x + 1, floorTiler.currentPosition.y);
                        break;
                }
                
                // Determine if the the tile that the tiler moved to is a valid tile coordinate.
                if([self.tiles isValidTileCoordinateAt:newPosition] &&
                   ![self.tiles isEdgeTileAt:newPosition] &&
                   [self.tiles tileTypeAt:newPosition] == MapTileTypeNone &&
                   currentFloorCount < self.maxFloorCount)
                {
                    floorTiler.currentPosition = newPosition;
                    [self.tiles setTileType:MapTileTypeFloor at:floorTiler.currentPosition];
                    currentFloorCount++;
                    
                    if ( [self randomNumberBetweenMin:0 andMax:100] <= self.roomProbability )
                    {
                        NSUInteger roomSizeX = [self randomNumberBetweenMin:self.roomMinSize.width
                                                                     andMax:self.roomMaxSize.width];
                        NSUInteger roomSizeY = [self randomNumberBetweenMin:self.roomMinSize.height
                                                                     andMax:self.roomMaxSize.height];
                        
                        currentFloorCount += [self generateRoomAt:floorTiler.currentPosition
                                                         withSize:CGSizeMake(roomSizeX, roomSizeY)];
                    }
                    
                    _exitPoint = [self convertMapCoordinateToWorldCoordinate:floorTiler.currentPosition];
                }
            }
            
            if ([self randomNumberBetweenMin:0 andMax:100] <= self.floorTilerSpawnProbability &&
                self.floorTilers.count < self.maxFloorTilerCount)
            {
                // If the random number is less than or equal to 50, then create a new floor tiler then
                // add it to the array of floor tilers.
                PixrFloorGeneration *newFloorTiler = [[PixrFloorGeneration alloc] initWithCurrentPosition:floorTiler.currentPosition andDirection:[self randomNumberBetweenMin:1 andMax:4]];
                
                [self.floorTilers addObject:newFloorTiler];
            }
            
        }];
    }
    
    // Convert the coordinates from the map coordinates to the world coordinates.
    _spawnPoint = [self convertMapCoordinateToWorldCoordinate:startPoint];
    
    // Show the map in the log as a text representation.
    NSLog(@"%@", [self.tiles description]);
}

/**
 * This method will determine where to generate the walls.
 */
- (void) generateWalls
{
    // 1
    for ( NSInteger y = 0; y < self.tiles.gridSize.height; y++ )
    {
        for ( NSInteger x = 0; x < self.tiles.gridSize.width; x++ )
        {
            CGPoint tileCoordinate = CGPointMake(x, y);
            
            // 2
            if ( [self.tiles tileTypeAt:tileCoordinate] == MapTileTypeFloor )
            {
                for ( NSInteger neighbourY = -1; neighbourY < 2; neighbourY++ )
                {
                    for ( NSInteger neighbourX = -1; neighbourX < 2; neighbourX++ )
                    {
                        // If the neighbour x and y are the origin (0,0) of the current tile, then move
                        // on to the next neighbours.
                        if ( !(neighbourX == 0 && neighbourY == 0) )
                        {
                            CGPoint coordinate = CGPointMake(x + neighbourX, y + neighbourY);
                            
                            // Set the floor tile to a wall if the coordinate returns a floor tile not
                            // yet set.
                            if ( [self.tiles tileTypeAt:coordinate] == MapTileTypeNone )
                            {
                                [self.tiles setTileType:MapTileTypeWall at:coordinate];
                                [self addCollisionWallAtPosition:coordinate withSize:CGSizeMake(self.tileSize, self.tileSize)];
                                
                            }
                        }
                    }
                }
            }
        }
    }
}


/**
 * This method will choose a number between a min and max from the arguments passed to it.
 */
-(NSInteger)randomNumberBetweenMin:(NSInteger)min
                            andMax:(NSInteger)max
{
    return min + arc4random() % (max - min);
}

/**
 * This method will determine how to generate the tiles.
 */
-(void)generateTiles
{
    // 1
    for ( NSInteger y = 0; y < self.tiles.gridSize.height; y++ )
    {
        for ( NSInteger x = 0; x < self.tiles.gridSize.width; x++ )
        {
            // 2
            CGPoint tileCoordinate = CGPointMake(x, y);
            
            // 3
            MapTileType tileType = [self.tiles tileTypeAt:tileCoordinate];
            
            // 4
            if (tileType != MapTileTypeNone)
            {
                // 5
                SKTexture *tileTexture = [self.tileAtlas textureNamed:[NSString stringWithFormat:@"%i", tileType]];
                SKSpriteNode *tile = [SKSpriteNode spriteNodeWithTexture:tileTexture];
                

                // 6
                tile.position = [self convertMapCoordinateToWorldCoordinate:CGPointMake(tileCoordinate.x, tileCoordinate.y)];
                
                // 7
                [self addChild:tile];
            }
        }
    }
}

- (NSUInteger)generateRoomAt:(CGPoint)position withSize:(CGSize)size
{
    NSUInteger numberOfFloorsGenerated = 0;
    
    for ( NSUInteger y = 0; y < size.height; y++)
    {
        for ( NSUInteger x = 0; x < size.width; x++ )
        {
            CGPoint tilePosition = CGPointMake(position.x + x, position.y + y);
            
            if ( [self.tiles tileTypeAt:tilePosition] == MapTileTypeInvalid )
            {
                continue;
            }
            
            if ( ![self.tiles isEdgeTileAt:tilePosition] )
            {
                if ( [self.tiles tileTypeAt:tilePosition] == MapTileTypeNone )
                {
                    [self.tiles setTileType:MapTileTypeFloor at:tilePosition];
                    
                    numberOfFloorsGenerated++;
                }
            }
        }
    }
    return numberOfFloorsGenerated;
}

-(CGPoint)convertMapCoordinateToWorldCoordinate:(CGPoint)mapCoordinate
{
    return CGPointMake(mapCoordinate.x * self.tileSize,  (self.tiles.gridSize.height - mapCoordinate.y) * self.tileSize);
}

@end
