//
//  BSLMyScene.h
//  Pixr
//
//  Created by Bryan Lor on 10/20/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//
#import <SpriteKit/SpriteKit.h>

@interface BSLMyScene : SKScene <SKPhysicsContactDelegate>

@end
