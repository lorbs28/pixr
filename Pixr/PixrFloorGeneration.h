//
//  PixrFloorGeneration.h
//  Pixr
//
//  Created by Bryan Lor on 12/15/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PixrFloorGeneration : NSObject

@property (nonatomic) CGPoint currentPosition;
@property (nonatomic) NSUInteger direction;

-(instancetype)initWithCurrentPosition:(CGPoint)currentPosition andDirection:(NSUInteger)direction;

@end
