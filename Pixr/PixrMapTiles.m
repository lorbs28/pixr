//
//  PixrMapTiles.m
//  Pixr
//
//  Created by Bryan Lor on 10/20/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//

#import "PixrMapTiles.h"

@interface PixrMapTiles ()
@property (nonatomic) NSInteger *tiles;
@end

@implementation PixrMapTiles

-(void)dealloc
{
    if ( self.tiles )
    {
        free(self.tiles);
        self.tiles = nil;
    }
}

-(instancetype)initWithGridSize:(CGSize)size
{
    if (( self = [super init] ))
    {
        _gridSize = size;
        _count = (NSUInteger) size.width * size.height;
        self.tiles = calloc(self.count, sizeof(NSInteger));
        NSAssert(self.tiles, @"Could not allocate memory for tiles");
    }
    return self;
}

-(MapTileType)tileTypeAt:(CGPoint)tileCoordinate
{
    NSInteger tileArrayIndex = [self tileIndexAt:tileCoordinate];
    
    if ( tileArrayIndex == -1 )
    {
        return MapTileTypeInvalid;
    }
    return self.tiles[tileArrayIndex];
}

-(void)setTileType:(MapTileType)type at:(CGPoint)tileCoordinate
{
    NSInteger tileArrayIndex = [self tileIndexAt:tileCoordinate];
    if ( tileArrayIndex == -1 )
    {
        return;
    }
    self.tiles[tileArrayIndex] = type;
}

/**
 * This method will check to see if the next tile that the floor tiler is going to move to
 * is a valid tile.
 */
-(BOOL)isValidTileCoordinateAt:(CGPoint)tileCoordinate
{
    /**
     * Check to see if the coordinates are within the grid.
     * I.e. if the x coordinate is less than 0, return NO (this is the outcome of !(YES)).
     */
    return !( tileCoordinate.x < 0 ||
             tileCoordinate.x >= self.gridSize.width ||
             tileCoordinate.y < 0 ||
             tileCoordinate.y >= self.gridSize.height );
}

/**
 * This method will check to see if the next tile that the floor tiler is going to move to is a tile
 * that is on the edge of the grid.
 */
-(BOOL)isEdgeTileAt:(CGPoint)tileCoordinate
{
    return ((NSInteger)tileCoordinate.x == 0 ||
            (NSInteger)tileCoordinate.x == (NSInteger)self.gridSize.width - 1 ||
            (NSInteger)tileCoordinate.y == 0 ||
            (NSInteger)tileCoordinate.y == (NSInteger)self.gridSize.height - 1);
}

-(NSInteger)tileIndexAt:(CGPoint)tileCoordinate
{
    if ( ![self isValidTileCoordinateAt:tileCoordinate] )
    {
        NSLog(@"Not a valid tile coordinate at %@", NSStringFromCGPoint(tileCoordinate));
        return MapTileTypeInvalid;
    }
    
    return ((NSInteger)tileCoordinate.y * (NSInteger)self.gridSize.width + (NSInteger)tileCoordinate.x);
}

-(NSString *)description
{
    NSMutableString *tileMapDescription = [NSMutableString stringWithFormat:@"<%@ = %p | \n",
                                           [self class], self];
    
    NSLog(@"%f", self.gridSize.height);
    
    for (NSInteger y = 0; y < (NSInteger)self.gridSize.height; y++)
    {
        [tileMapDescription appendString:[NSString stringWithFormat:@"[%i]", y]];
        
        for ( NSInteger x = 0; x < (NSInteger)self.gridSize.width; x++ )
        {
            [tileMapDescription appendString:[NSString stringWithFormat:@"%i",
                                              [self tileTypeAt:CGPointMake(x, y)]]];
        }
        [tileMapDescription appendString:@"\n"];
    }

    
    return [tileMapDescription stringByAppendingString:@">"];
}

@end
