//
//  BSLViewController.m
//  Pixr
//
//  Created by Bryan Lor on 10/16/13.
//  Copyright (c) 2013 Bryan Lor. All rights reserved.
//

#import "BSLViewController.h"
#import "BSLMyScene.h"

@interface BSLViewController ()

@property (nonatomic) BOOL didLayoutSubviews;

@end

@implementation BSLViewController

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];

    if ( !self.didLayoutSubviews )
    {
    // Configure the view.
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
    
    NSLog(@"height: %f, width: %f", skView.bounds.size.height, skView.bounds.size.width);
    
    // Create and configure the scene.
    SKScene * scene = [BSLMyScene sceneWithSize:skView.bounds.size];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:scene];
    }
    
    self.didLayoutSubviews = YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

@end
