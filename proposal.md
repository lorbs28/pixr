# Project Proposal (More information incoming!)

**App type:** 

Game

**App genre:** 

RPG (Role-playing game)

**What is this app?**

It'll be a 2D game in the style of retro RPG games like Final Fantasy!

**Why this app?**

I've always been a HUGE fan of RPGs, particularly the old school ones when things were simpler, ever since I was a kid.
But one thing I wasn't a big fan of was having to read so much of the dialog, so I would skip through the dialog as much as possible,
thus missing out on the entire story in order to get to the more fun parts like:

* Battles!
* Mini-games!
* Collecting equipment!
* Easter eggs!
* Challenges!

**How will this game be different from others?**

It'll be focused less on a backstory and more about what you can do in the game.  An RPG that gets straight to the point.
There will probably be a backstory, but it won't be as big as the mechanics.

**Why retro graphics?**

I used to be good at drawing cartoon/anime characters, but after years of not keeping up with it, retro low-bit graphics will be easier
for me to accomplish for both character and environment designs.

**What about sound/music?**

MIDI sounds and music all the way!  Nothing like MIDI paired with retro-style graphics.  I'm not great at creating music so 
I will probably either try to create my own or have a friend who's good at creating electronic music create something for me to use.

**Similar apps**

* [Chrono Trigger](https://itunes.apple.com/us/app/chrono-trigger/id479431697) - Console port
* [Lunar Star Silver Story Touch](https://itunes.apple.com/us/app/lunar-silver-star-story-touch/id492598036?mt=8) - Console port
* [Final Fantasy](https://itunes.apple.com/us/app/final-fantasy/id354972939) - Console port
* [Final Fantasy II](https://itunes.apple.com/us/app/final-fantasy-ii/id354974729) - Console port
  
All the above apps have a few things in common:

* A hero/protagonist v.s. evil, bad guy/antagonist.
* Something tragic happens at the beginning of the game.
* Similar gameplay: turn-based

## Proposal Overview

* Game focused primarily on classic RPG mechanics
	* Battle system
	* Random monster encounters
	* Collecting equipment/trinkets
	* Mini-games (future)
* Device support
	* iPod Touch
	* iPhone
	* iPad (future)
* Game algorithms
	* Tweak the various readily available RPG algorithms in the wild.
	* Come up with own custom algorithm (future possibly).

### The idea is to keep it as simple as possible to begin with.  

* The bestiary will contain very few monsters/beasts at first.
* The character stats will be fairly simple.
	* Strength, intellect, stamina, armor defense
* With character stats being fairly simple, the algorithm for determining damage given/taken 
	will reflect the simplicity also.
* Equipment/trinkets will have randomly generated stats and level requirements.

---














