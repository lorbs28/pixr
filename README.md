# Pixr

---

## Description

This is an iOS app that I was working on as my semester project.  It was an early attempt at making a game demo entirely with SpriteKit with movement using on-screen controls and featuring collision amongst other features.  This app was started when iOS 7 first came out and SpriteKit was still fairly new.
